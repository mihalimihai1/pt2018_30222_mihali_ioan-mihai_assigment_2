package PT2018.demo.Project2;

import java.util.Random;

public class Simulare implements Runnable{
	public App frame= new App();
	public int tminServire;
	public int tmaxServire;
	public int tminSosire;
	public int tmaxSosire;
	public int nrCozi;
	public int interval;
	public int timpcurent;
	private Thread t;
	public int ind;
	public int sos1;
	private boolean running;
	public boolean ok;
	public Server[] s;
	public Random random;
	
	public Simulare(int x, int y, int z, int p, int q, int r, App f){
		this.tminServire=x;
		this.tmaxServire=y;
		this.tminSosire=z;
		this.tmaxSosire=p;
		this.nrCozi=q;
		this.interval=r;
		this.frame=f;
		
		random=new Random();
		t=new Thread(this);
		running=false;
		s=new Server[nrCozi];
		for(int i=0;i<nrCozi;i++){
			s[i]=new Server(i,frame);
		}
	}
	public void run() {
		System.out.println("Simularea a inceput");
		int coada;
		int serv;
		int sos;
		while(timpcurent<interval){
				timpcurent=timpcurent+random(tminSosire,tmaxSosire);
				try {
					Thread.sleep(random(tminSosire,tmaxSosire)*1000);
				} catch (InterruptedException e) {
						e.printStackTrace();
					}
				serv=random(tminServire,tmaxServire);
				sos=random(tminSosire,tmaxSosire);
				Client x= new Client(sos,serv);
				coada=clienti_min();
				s[coada].addClient(x);
				frame.updateadd(String.valueOf(s[coada].i+1),sos,coada+1, coada);
				System.out.println("Clientul a fost adaugat in coada "+(coada+1));
			}
		for(int i=0;i<s.length;i++){
			s[i].stop();
		}
		System.out.println("Simularea s-a terminat");
	}
  
	public void start() {
		running = true;
		t.start();
		for(int i=0;i<s.length;i++)
			s[i].start();
	}
	public int random(int mini, int maxi) {
		return (random.nextInt(maxi-mini+1)+mini);
	}
	
	public int clienti_min() {
		int min=s[0].coada.size();
		int unde=0;
		for(int i=1;i<s.length;i++)
		{
			if(s[i].coada.size()<min) {
				min=s[i].coada.size();
				unde=i;
			}
		}
		return unde;
	}
}
