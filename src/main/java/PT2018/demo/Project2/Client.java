package PT2018.demo.Project2;

public class Client {
	int timp_sosire;
	int timp_procesare;
	
	public Client(int timp_sosire, int timp_procesare) {
		this.timp_sosire=timp_sosire;
		this.timp_procesare=timp_procesare;
	}
	public int gettimps() {
		return this.timp_sosire;
	}
	public int gettimpp() {
		return this.timp_procesare;
	}
	public void settimps(int x) {
		this.timp_sosire=x;
	}
	public void settimpp(int y) {
		this.timp_procesare=y;
	}
}
