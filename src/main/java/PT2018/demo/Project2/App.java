package PT2018.demo.Project2;
import java.awt.EventQueue;
import java.awt.*;
import javax.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.*;
import java.util.Random;

public class App extends JFrame{
	public JPanel frame;
	public JTextField[] output = new JTextField[5];
	public JTextField[] param = new JTextField[6];
	public JTextArea text;
	public JScrollPane scroll;
	public JButton start;
	public String[] sir;
	private String coadaConvert;
	ArrayList<String> oameniC1=new ArrayList<String>();
    ArrayList<String> oameniC2=new ArrayList<String>();
	ArrayList<String> oameniC3=new ArrayList<String>();
	ArrayList<String> oameniC4=new ArrayList<String>();
	ArrayList<String> oameniC5=new ArrayList<String>();
    public static void main( String[] args ){
    	EventQueue.invokeLater(new Runnable() {
			public void run() {
				try{
					App frame = new App();
					frame.setVisible(true);
				} 
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
    }
    public App() {
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(800, 800, 800, 800);
		frame = new JPanel();
		setContentPane(frame);
		frame.setLayout(null);
		
		JLabel NumarCozi = new JLabel("Numar cozi(nu mai mult de 5):");
		NumarCozi.setBounds(10, 650, 170, 14); //x y lungime latime
		frame.add(NumarCozi);
		
		JLabel TimpMinimSosire = new JLabel("Timp minim sosire:");
		TimpMinimSosire.setBounds(10, 670, 150, 14);
		frame.add(TimpMinimSosire);
		
		JLabel TimpMaximSosire = new JLabel("Timp maxim sosire:");
		TimpMaximSosire.setBounds(230, 670, 150, 14);
		frame.add(TimpMaximSosire);
		
		JLabel TimpMinimServire = new JLabel("Timp minim servire:");
		TimpMinimServire.setBounds(10, 690, 150, 14);
		frame.add(TimpMinimServire);
		
		JLabel TimpMaximServire = new JLabel("Timp maxim servire:");
		TimpMaximServire.setBounds(230, 690, 150, 14);
		frame.add(TimpMaximServire);
		
		JLabel lblIntervalSimulare = new JLabel("Interval simulare:");
		lblIntervalSimulare.setBounds(230, 650, 100, 14);
		frame.add(lblIntervalSimulare);
		
		JLabel lblThread = new JLabel("Coada1:");
		lblThread.setBounds(10, 111, 68, 14);
		frame.add(lblThread);
		
		JLabel lblServer = new JLabel("Coada2:");
		lblServer.setBounds(10, 136, 68, 14);
		frame.add(lblServer);
		
		JLabel lblServer_1 = new JLabel("Coada3:");
		lblServer_1.setBounds(10, 161, 68, 14);
		frame.add(lblServer_1);
		
		JLabel lblServer_2 = new JLabel("Coada4:");
		lblServer_2.setBounds(10, 186, 68, 14);
		frame.add(lblServer_2);
		
		JLabel lblThread_1 = new JLabel("Coada5:");
		lblThread_1.setBounds(10, 211, 68, 14);
		frame.add(lblThread_1);
		
		output[0] = new JTextField();
		output[0].setBounds(88, 108, 318, 20);
		frame.add(output[0]);
		output[0].setColumns(10);
		
		output[1] = new JTextField();
		output[1].setBounds(88, 133, 318, 20);
		frame.add(output[1]);
		output[1].setColumns(10);
		
		output[2] = new JTextField();
		output[2].setBounds(88, 158, 318, 20);
		frame.add(output[2]);
		output[2].setColumns(10);
		
		output[3] = new JTextField();
		output[3].setBounds(88, 183, 318, 20);
		frame.add(output[3]);
		output[3].setColumns(10);
		
		output[4] = new JTextField();
		output[4].setColumns(10);
		output[4].setBounds(88, 208, 318, 20);
		frame.add(output[4]);
		
		param[0] = new JTextField();
		param[0].setBounds(180, 650, 40, 20);
		frame.add(param[0]);
		param[0].setColumns(10);
		
		param[1] = new JTextField();
		param[1].setColumns(10);
		param[1].setBounds(180, 670, 40, 20);
		frame.add(param[1]);
		
		param[2] = new JTextField();
		param[2].setColumns(10);
		param[2].setBounds(350, 670, 40, 20);
		frame.add(param[2]);
		
		param[3] = new JTextField();
		param[3].setColumns(10);
		param[3].setBounds(180, 690, 40, 20);
		frame.add(param[3]);
		
		param[4] = new JTextField();
		param[4].setColumns(10);
		param[4].setBounds(350, 690, 40, 20);
		frame.add(param[4]);
		
		param[5] = new JTextField();
		param[5].setColumns(10);
		param[5].setBounds(350, 650, 40, 20);
		frame.add(param[5]);
		
		text = new JTextArea(10,400);
		text.setEditable(false);
		scroll = new JScrollPane(text);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setBounds(30,300,450,267);
		frame.add(scroll);
		frame.setVisible(true);
		
		start = new JButton("Incepe Simularea");
		start.setBounds(450,650,300,80);
		frame.add(start);
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				try {
					int nr_cozi = Integer.parseInt(param[0].getText());
					int timp_minim_sosire = Integer.parseInt(param[1].getText());
					int timp_maxim_sosire = Integer.parseInt(param[2].getText());
					int timp_minim_servire = Integer.parseInt(param[3].getText());
					int timp_maxim_servire = Integer.parseInt(param[4].getText());
					int interval_simulare = Integer.parseInt(param[5].getText());
					Simulare sim = new Simulare(timp_minim_servire,timp_maxim_servire,timp_minim_sosire,timp_maxim_sosire,nr_cozi,interval_simulare,App.this);
					sim.start();
				}
				catch(NumberFormatException e){
					System.out.printf("Eroare boss!");
				}
			}
		});
		JLabel ps = new JLabel("# Strategia aleasa este in functie de locuri.");
		ps.setBounds(30,40,300,20);
		frame.add(ps);
		
    }
    private String convertToString(ArrayList<String> array)
    {
    	coadaConvert="";
    	for(String s: array)
    	{
    	    coadaConvert+=s	;
    	}
    	return coadaConvert;
    }
     void setare(int b){
		String y="( ͡ᵔ ͜ʖ ͡ᵔ )";
			if(b==0) {
			oameniC1.add(y);
			output[b].setText(convertToString(oameniC1));
			}
			
			if(b==1) {
				oameniC2.add(y);
				output[b].setText(convertToString(oameniC2));}

			if(b==2) {
				oameniC3.add(y);
				output[b].setText(convertToString(oameniC3));}

			if(b==3) {
				oameniC4.add(y);
				output[b].setText(convertToString(oameniC4));}

			if(b==4) {
				oameniC5.add(y);
				output[b].setText(convertToString(oameniC5));}

		
		System.out.println(b+" "+sir);
	}
     
     void stergere(int b)
     {
    		if(b==0) {
    			oameniC1.remove(0);
    			output[b].setText(convertToString(oameniC1));
    			}
    			
    			if(b==1) {
    				oameniC2.remove(0);
    				output[b].setText(convertToString(oameniC2));}

    			if(b==2) {
    				oameniC3.remove(0);
    				output[b].setText(convertToString(oameniC3));}

    			if(b==3) {
    				oameniC4.remove(0);
    				output[b].setText(convertToString(oameniC4));}

    			if(b==4) {
    				oameniC5.remove(0);
    				output[b].setText(convertToString(oameniC5));}
     }
    	void updateadd(String s,int a,int b, int index){
    		text.append("Un client a fost adaugat in coada: "+s+" cu AT: "+a+" si ST: "+b +"\n");
    		setare(index);
    	}
    	void updateremove(String s,int y, int b, int index){
    		text.append("Clientul iesit din coada "+s + " dupa " + y + " secunde de asteptare"+"\n");
    		stergere(index);
    	}
}
